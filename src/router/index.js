import Login from '@/components/Login'
import Chat from '@/components/Chat'

const routes = [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat,
      props: true,
      // beforeEnter: (to, from, next) => {
      //   if(to.params.name){
      //     next();
      //   } else {
      //     next({name: 'Welcome'})
      //   }
      // }
    }
  ]

  export default routes
