import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routes from './router/index'
import moment from 'moment'

Vue.config.productionTip = false
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
})

Vue.filter('chatDate', (created) => {
  return moment(created).format('LLLL')
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
